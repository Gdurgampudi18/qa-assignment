package com.leastplan.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.*;

public class VehicleFilterTests {
	
	WebDriver driver;
	
	@Parameters("browser")
	@BeforeTest
	public void setup(String browser){
		
		if(browser.equals("chrome")){
			System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
			driver = new ChromeDriver();
		}else if(browser.equals("firefox")){
			System.setProperty("webdriver.gecko.driver", "drivers/geckodriver.exe");
			driver = new FirefoxDriver();
		}
		
		driver.get("https://www.leaseplan.com/en-be/business/showroom/");
	
	}
	
	@Test(priority=1)
	public void vehicleFilter() throws Exception{
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("//button[@title='Accept Cookies']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//h3[text()='Popular filters'])[2]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[@data-e2e-id='Best deals']//span")).click();
		driver.findElement(By.xpath("//*[@id='app']/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[2]/div/div[2]/button")).click();
		Thread.sleep(2000);

		driver.findElement(By.xpath("//h3/span[text()='More filters']")).click();
		
		boolean actual = driver.findElement(By.xpath("//span[text()='17 to choose from']")).isDisplayed();
		
		Assert.assertEquals(actual, true);
		
        WebElement slider1 = driver.findElement(By.xpath("(//div[@class='rc-slider-handle'])[2]"));
        Actions move1 = new Actions(driver);
        Action action1 = (Action) move1.dragAndDropBy(slider1, 90, 0).build();
        action1.perform();
	
	     driver.findElement(By.xpath("/html/body/div[7]/div/div/div/div/div/div[4]/button")).click();
	     Thread.sleep(4000);
		 boolean actualResultAfterFilter = driver.findElement(By.xpath("//span[text()='4 to choose from']")).isDisplayed();
		 Assert.assertEquals(actualResultAfterFilter, true);
	
	}
	
	@Test(priority=2)
	public void sortingPrice() throws Exception{
		
		String priceBeforeFilter=driver.findElement(By.xpath("//*[@id='Filtered Cars Grid']/div[1]/div/div[1]/div/div/a/div[4]/div/div[1]/span")).getText();
		
		Select priceDropdown=new  Select(driver.findElement(By.xpath("(//select[@id='orderBy_input'])[2]")));
		priceDropdown.selectByVisibleText("Price (low-high)");
	
		Thread.sleep(2000);
		String priceAfterFilter=driver.findElement(By.xpath("//*[@id='Filtered Cars Grid']/div[1]/div/div[1]/div/div/a/div[4]/div/div[1]/span")).getText();
		
		Assert.assertNotEquals(priceBeforeFilter, priceAfterFilter);
		
	}
	
	
	

}
